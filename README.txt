Title of the Project : Graphical Calculator
Name of the Author : Nishant Ingle
MIS : 111608030

Description of the Project :

1) This project is done using GTK library and with support
   of Glade User Interface Designer.
2) Maximum precision is same as precision of double.
3) Maximum length is same as maximum length of long long int.
4) Scientific Mode is implemented.
5) List of functions with corresponding operator :

				(1) Addition (Operator : +)
				(2) Subtraction (Operator : -)
				(3) Multiplication (Operator : *)
 				(4) Division (Operator : /)
				(5) Modulo (Operator : %)
				(6) Power (Operator : ^)
				(7) Square-Root (Operator : √)
				(8) Absolute value or Modulus (Operator : |x|)
				(9) Factorial of an integer (Operator : x!)
			       (10) Sine (Operator : sin)
			       (11) Cosine (Operator : cos)
			       (12) Tangent (Operator : tan)
			       (13) Hyperbolic Sine (Operator : sinh)
			       (14) Hyperbolic Cosine (Operator : cosh)
			       (15) Hyperbolic Tangent (Operator : tanh)
			       (16) Common Logarithm (Operator : log)
			       (17) Natural Logarithm(Operator : ln)
                               (18) Into 10 to the power (Operator : *10^)


6) Priority Order:
				(1) Replacement of π and e with their respective vaules.
				(2) |x|
				(3) ln
				(4) log
				(5) sinh
				(6) cosh
				(7) tanh
				(8) sin
				(9) cos
			       (10) tan
			       (11) x!
			       (12) ^
			       (13) *10^y
			       (14) √
			       (15) %
			       (16) *
			       (17) /
			       (18) -
			       (19) +

7) This Program uses string processing to evaluate values of infix expressions.

8) String is converted to double and sometimes long long values and operations
   are done on these values which result into answer.

9) This answer is again checked if their is any operator remaining to be 
   processed.

10) If no then this answer is saved as final result in the form of a string.

11) This Program works even if there are white spaces because it contains a 
    function to remove white spaces.

12) Text entry is used to get input, direct input from keyboard is possible 
    in this entry and it gets empty when 'del' (delete button) is clicked.

13) Input from buttons can be added to the text entry.





