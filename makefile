all: main.o
	gcc main.o `pkg-config --cflags --libs gtk+-3.0` -export-dynamic -o project -lm
    
main.o: src/main.c
	gcc -c  -Wall src/main.c `pkg-config --cflags --libs gtk+-3.0` -o main.o 
    
clean:
	rm -f *.o project
