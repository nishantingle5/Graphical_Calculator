/****Graphical Calculator****
 *Note : For better readabilty of the code set tab width to 2.
 */
#include<gtk/gtk.h>
#include<math.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<limits.h>
#include"main.h"
/*Declaring GTK Widget pointers which are global in nature so 
 *that they can be used for operations in different functions.
 */
GtkWidget *g_lbl_entry, *g_lbl_label;
char res[50] = {0};
int main(int argc, char *argv[]){
    GtkBuilder      *builder; 
		GtkWidget       *window;
    gtk_init(&argc, &argv);
    builder = gtk_builder_new();
    gtk_builder_add_from_file (builder, "interface/window_main.glade", NULL);
    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    gtk_builder_connect_signals(builder, NULL);
    /*get pointer to the text entry*/
    g_lbl_entry = GTK_WIDGET(gtk_builder_get_object(builder, "entry"));
    /*get pointer to the text label*/
    g_lbl_label = GTK_WIDGET(gtk_builder_get_object(builder, "label"));
    g_object_unref(builder);
    gtk_widget_show(window);                
    gtk_main();
    return 0;
}
/*called when zero is clicked*/
void on_zero_clicked(){
	const char *text, *zero = "0";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, zero);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
} 
/*called when one is clicked*/
void on_one_clicked(){		
	const char *text, *one = "1";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, one);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when two is clicked*/
void on_two_clicked(){
	const char *text, *two = "2";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, two);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when three is clicked*/
void on_three_clicked(){
	const char *text, *three = "3";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, three);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when four is clicked*/
void on_four_clicked(){
	const char *text, *four = "4";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, four);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when five is clicked*/
void on_five_clicked(){
	const char *text, *five = "5";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, five);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when six is clicked*/
void on_six_clicked(){
	const char *text, *six = "6";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, six);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when seven is clicked*/
void on_seven_clicked(){
	const char *text, *seven = "7";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, seven);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);	
}
/*called when eight is clicked*/
void on_eight_clicked(){
	const char *text, *eight = "8";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, eight);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when nine is clicked*/
void on_nine_clicked(){
	const char *text, *nine = "9";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, nine);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when plus is clicked*/
void on_plus_clicked(){
	const char *text, *plus = "+";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, plus);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when minus is clicked*/
void on_minus_clicked(){
	const char *text, *minus = "-";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, minus);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
} 
/*called when multiply is clicked*/
void on_multiply_clicked(){
	const char *text, *multiply = "*";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, multiply);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
} 
/*called when divide is clicked*/
void on_divide_clicked(){
	const char *text, *divide = "/";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, divide);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
} 
/*called when % is clicked*/
void on_mod_clicked(){
	const char *text, *mod = "%";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, mod);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
} 
/*called when sinh is clicked*/
void on_sinh_clicked(){
	const char *text, *sinh = "sinh";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, sinh);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when cosh is clicked*/
void on_cosh_clicked(){
	const char *text, *cosh = "cosh";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, cosh);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when tanh is clicked*/
void on_tanh_clicked(){
	const char *text, *tanh = "tanh";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, tanh);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
} 
/*called when sin is clicked*/
void on_sin_clicked(){
	const char *text, *sin = "sin";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, sin);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when cos is clicked*/
void on_cos_clicked(){
	const char *text, *cos = "cos";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, cos);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when tan is clicked*/
void on_tan_clicked(){
	const char *text, *tan = "tan";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, tan);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when √ is clicked*/
void on_sqrt_clicked(){
	const char *text, *sqrt = "sqrt";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, sqrt);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when ^ is clicked*/
void on_pow_clicked(){
	const char *text, *pow = "^";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, pow);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when *10^y is clicked*/
void on_into_ten_pow_y_clicked(){
	const char *text, *tpy = "*10^";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, tpy);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when x! is clicked*/
void on_fact_clicked(){
	const char *text, *fact = "!";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, fact);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when log is clicked*/
void on_log_clicked(){
	const char *text, *log = "log";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, log);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when ln is clicked*/
void on_ln_clicked(){
	const char *text, *ln = "ln";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, ln);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when π is clicked*/
void on_pi_clicked(){
	const char *text, *pi = "pi";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, pi);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when e is clicked*/
void on_e_clicked(){
	const char *text, *e = "e";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, e);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when |x| is clicked*/
void on_modulus_clicked(){
	const char *text, *mod = "|";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, mod);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when ( is clicked*/
void on_in_clicked(){
	const char *text, *in = "(";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, in);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when ) is clicked*/
void on_out_clicked(){
	const char *text, *out = ")";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, out);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when . is clicked*/
void on_dot_clicked(){
	const char *text, *dot = ".";
	char *new_text = malloc(sizeof(char) * 50);
	text = gtk_entry_get_text(GTK_ENTRY(g_lbl_entry));
	strcpy(new_text, text);
	strcat(new_text, dot);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*Function to check if passed character
 *can be a part of an integer or not.
 */
int isDigit(char i){
	switch(i){
		case '1' : case '2' : case '3' :
		case '4' : case '5' : case '6' :
		case '7' : case '8' : case '9' :
		case '0' : case '.' : case '-' :
			return 1;
		default :
			return 0;
	}
}
/*Function to replace string starting at index s 
 *and terminating at index e in temp by text
 */
void expreplace(char *temp, int s, int e, char *text){
	int i, j,k = 0;
	char next[1024] = {0};
	if(s > e || e >= strlen(temp))
		return;
	for(i = 0; i < strlen(temp); i++){
		if(i == s){
			for(j = 0; j < strlen(text); j++)
				next[k++] = text[j];
			if(e != strlen(temp))
				i = e + 1;
			else 
				i++;

		}
		next[k++] = temp[i];

	}
	next[k] = '\0';
	strcpy(temp, next);	
}
/*Function to check whether no. of opening 
 *and closing braces are equal or not.
 */ 
int braces_checker(char *p){
	int i, in = 0, out = 0;
	for(i = 0; i < strlen(p); i++){
		if(p[i] == '(')
			in++;
		if(p[i] == ')')
			out++;
	}
	if(in == out)
		return 0;
	return 1;
}
/*Function to evaluate an infix expressions.
 *Limitation : Result should not be out of
 *bounds to the maximum length defined in 
 *README.txt
 */
char *evaluator(char *temp){
	int flag = 0, i, s = 0, e = -1, j = 0, k = 0;
	char n[20] = {0};
	double res = 0;
	if(braces_checker(temp))
		return "Invalid Input";
	/*For pi */
	while(1){
		if(strlen(temp) < 2)
			break;
		for(i = 0; i < strlen(temp) - 1; i++){
			if(temp[i] == 'p' && temp[i + 1] == 'i'){
					s = i;	
					flag = 0;
					e = i + 1;
					break;				
			}
			else 
				flag = 1;
		}
		if(flag == 1)
			break;
		expreplace(temp, s, e, "3.141592654");
	}
	flag = 0;
	s = 0;
	e = -1;
	/*For e*/
	while(1){
		if(strlen(temp) < 1)
			break;
		for(i = 0; i < strlen(temp); i++){
			if(temp[i] == 'e'){
				s = i;
				flag = 0;
				e = i;
				break;
			}
			else {
				flag = 1;
				s = 0;
				e = -1;
			}
		}
		if(flag == 1)
			break;
		expreplace(temp, s, e, "2.718281828");
	}
	flag = 0;
	/*For |x|*/
	while(1){
		if(strlen(temp) < 3)
			break;
		for(i = 0; i < strlen(temp); i++){
			if(temp[i] == '|' && isDigit(temp[i + 1])){
				s = i;
				for(j = i + 2; j < strlen(temp); j++){
					if(temp[j] == '|'  && isDigit(temp[j - 1])){
						e = j;
						flag = 0;
						break;
					}
					else
						flag = 1;
				}
				if(flag == 0)
					break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		k = 0;
		for(j = s + 1; j < e && j < strlen(temp); j++)
			n[k++] = temp[j];
		n[k] = '\0';
		strcpy(n, evaluator(n));
		res = atof(n);
		res = fabs(res);
		sprintf(n, "%lf", res);	
		expreplace(temp, s, e, n);
	}
	/*For ln()*/ 
	while(1){
		if(strlen(temp) < 3)
			break;
		for(i = 0; i < strlen(temp) - 2; i++){
			if(temp[i] == 'l' && temp[i + 1] == 'n' && isDigit(temp[i + 2])){
					s = i;
					flag = 0;
					break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 2; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 2; i <= e; i++)
			n[k++] = temp[i];
		res = atof(n);
		n[k] = '\0';
		res = log(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For log()*/ 
	while(1){
		if(strlen(temp) < 4)
			break;
		for(i = 0; i < strlen(temp) - 3; i++){
			if(temp[i] == 'l' && temp[i + 1] == 'o' && temp[i + 2] == 'g' && isDigit(temp[i + 3])){
					s = i;
					flag = 0;
					break;
			}
			else
				flag = 1;
		}
		if(flag == 1 || strlen(temp) < 4)
			break;
		for(i = s + 3; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 3; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = log10(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For sinh()*/ 
	while(1){
		if(strlen(temp) < 5)
			break;
		for(i = 0; i < strlen(temp) - 4; i++){
			if(temp[i] == 's' && temp[i + 1] == 'i' && temp[i + 2] == 'n' && temp[i + 3] == 'h' && isDigit(temp[i + 4])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 4; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 4; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = sinh(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For cosh()*/ 
	while(1){
		if(strlen(temp) < 5)
			break;
		for(i = 0; i < strlen(temp) - 4; i++){
			if(temp[i] == 'c' && temp[i + 1] == 'o' && temp[i + 2] == 's' && temp[i + 3] == 'h' && isDigit(temp[i + 4])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 4; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 4; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = cosh(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For tanh()*/ 
	while(1){
		if(strlen(temp) < 5)
			break;
		for(i = 0; i < strlen(temp) - 4; i++){
			if(temp[i] == 't' && temp[i + 1] == 'a' && temp[i + 2] == 'n' && temp[i + 3] == 'h' && isDigit(temp[i + 4])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 4; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 4; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = tanh(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For sin()*/ 
	while(1){
		if(strlen(temp) < 4)
			break;
		for(i = 0; i < strlen(temp) - 3; i++){
			if(temp[i] == 's' && temp[i + 1] == 'i' && temp[i + 2] == 'n' && isDigit(temp[i + 3])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 3; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 3; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = res * (0.017453293);
		res = sin(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For cos()*/ 
	while(1){
		if(strlen(temp) < 4)
			break;
		for(i = 0; i < strlen(temp) - 3; i++){
			if(temp[i] == 'c' && temp[i + 1] == 'o' && temp[i + 2] == 's' && isDigit(temp[i + 3])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 3; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 3; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = res * (0.017453293);
		res = cos(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For tan()*/ 
	while(1){
		if(strlen(temp) < 4)
			break;
		for(i = 0; i < strlen(temp) - 3; i++){
			if(temp[i] == 't' && temp[i + 1] == 'a' && temp[i + 2] == 'n' && isDigit(temp[i + 3])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 3; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 3; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		if((int)res % 90 == 0)
			return "Undefined";
		res = res * (0.017453293);
		res = tan(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For x!*/
	while(1){
		if(strlen(temp) < 2 || temp[0] == '!')
			break;
		long long fact = 1; 
		int number = 1;
		for(i = 1; i < strlen(temp); i++){
			if(temp[i] == '!' && isDigit(temp[i - 1])){
				e = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = e - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		k = 0;
		for(i = s; i < e; i++)
				n[k++] = temp[i];
		n[k] = '\0';
		number = atoi(n);
		if(number > 20)
			return "Limit Exceeded";
		for(i = 2; i <= number; i++)
			fact = fact * (long long)i;
		sprintf(n, "%lld", fact);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For Power*/ 
	while(1){
		if(strlen(temp) < 3 || temp[0] == '^' || temp[strlen(temp)] == '^')
			break;
		int middle = 0;
		float one = 0, two = 0;
		char num1[20] = {0}, num2[20] = {0};
		for(i = 1; i < strlen(temp) - 1; i++){
			if(temp[i] == '^' && isDigit(temp[i + 1]) && isDigit(temp[i - 1])){
				middle = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = middle - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		for(i = middle + 1; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s; i < (int)middle; i++)
				num1[k++] = temp[i];
		num1[k] = '\0';
		k = 0;
		for(i = (int)middle + 1; i <= e; i++)
				num2[k++] = temp[i];
		num2[k] = '\0';
		k = 0;
		one = atof(num1);
		two = atof(num2);
		res = pow(one, two);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For sqrt()*/ 
	while(1){
		if(strlen(temp) < 5 || temp[strlen(temp)] == 't')
			break;
		for(i = 0; i < strlen(temp) - 4; i++){
			if(temp[i] == 's' && temp[i + 1] == 'q' && temp[i + 2] == 'r' && temp[i + 3] == 't' && isDigit(temp[i + 4])){
				s = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		
		for(i = s + 4; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s + 4; i <= e; i++)
			n[k++] = temp[i];
		n[k] = '\0';
		res = atof(n);
		res = sqrt(res);
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For Modulus (%)*/ 
	while(1){
		if(strlen(temp) < 3 || temp[0] == '%' || temp[strlen(temp)] == '%')
			break;
		int middle = 0;
		float one = 0, two = 0;
		char num1[20] = {0}, num2[20] = {0};
		for(i = 1; i < strlen(temp) - 1; i++){
			if(temp[i] == '%' && isDigit(temp[i + 1]) && isDigit(temp[i - 1])){
				middle = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = middle - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		for(i = middle + 1; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s; i < (int)middle; i++)
				num1[k++] = temp[i];
		num1[k] = '\0';
		k = 0;
		for(i = (int)middle + 1; i <= e; i++)
				num2[k++] = temp[i];
		num2[k] = '\0';
		k = 0;
		one = atof(num1);
		two = atof(num2);
		if(one < 0 && two < 0){
			one = one * (-1);
			two = two * (-1);
		}
		res = (int)one % (int)two;
		if((one < 0 && two > 0) || (one > 0 && two < 0))
			res = res * (-1);
		sprintf(n, "%lld", (long long)res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For Division*/ 
	while(1){
		if(strlen(temp) < 3 || temp[0] == '/' || temp[strlen(temp)] == '/')
			break;
		int middle = 0;
		float one = 0, two = 0;
		char num1[20] = {0}, num2[20] = {0};
		for(i = 1; i < strlen(temp) - 1; i++){
			if(temp[i] == '/' && isDigit(temp[i + 1]) && isDigit(temp[i - 1])){
				middle = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = middle - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		for(i = middle + 1; isDigit(temp[i]) && i < strlen(temp); i++){
			if(i != middle + 1 && temp[i] == '-')
				break;
			e = i;
		}
		k = 0;
		for(i = s; i < (int)middle; i++)
				num1[k++] = temp[i];
		num1[k] = '\0';
		k = 0;
		for(i = (int)middle + 1; i <= e; i++)
				num2[k++] = temp[i];
		num2[k] = '\0';
		k = 0;
		one = atof(num1);
		two = atof(num2);
		res = one / two;
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For Multiplication*/ 
	while(1){
		if(strlen(temp) < 3 || temp[0] == '*' || temp[strlen(temp)] == '*')
			break;
		int middle = 0;
		float one = 0, two = 0;
		char num1[20] = {0}, num2[20] = {0};
		for(i = 1; i < strlen(temp) - 1; i++){
			if(temp[i] == '*' && isDigit(temp[i + 1])  && isDigit(temp[i - 1])){
				middle = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = middle - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		for(i = middle + 1; isDigit(temp[i]) && i < strlen(temp); i++){
			if(i != middle + 1 && temp[i] == '-')
				break;
			e = i;
		}
		k = 0;
		for(i = s; i < (int)middle; i++)
				num1[k++] = temp[i];
		num1[k] = '\0';
		k = 0;
		for(i = (int)middle + 1; i <= e; i++)
				num2[k++] = temp[i];
		num2[k] = '\0';
		k = 0;
		one = atof(num1);
		two = atof(num2);
		res = one * two;
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For Subtraction*/ 
	while(1){
		if(strlen(temp) < 3 || temp[strlen(temp)] == '-')
			break;
		int middle = 0;
		float one = 0, two = 0;
		char num1[20] = {0}, num2[20] = {0};
		for(i = 1; i < strlen(temp) - 1; i++){
			if(temp[i] == '-'){
				middle = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = middle - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		for(i = middle + 1; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s; i < (int)middle; i++)
				num1[k++] = temp[i];
		num1[k] = '\0';
		k = 0;
		for(i = (int)middle + 1; i <= e; i++)
				num2[k++] = temp[i];
		num2[k] = '\0';
		k = 0;
		one = atof(num1);
		two = atof(num2);
		res = one - two;
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	/*For Addition*/ 
	while(1){
		if(strlen(temp) < 3 || temp[strlen(temp)] == '+')
			break;
		int middle = 0;
		float one = 0, two = 0;
		char num1[20] = {0}, num2[20] = {0};
		for(i = 1; i < strlen(temp) - 1; i++){
			if(temp[i] == '+'){
				middle = i;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if(flag == 1)
			break;
		for(i = middle - 1; isDigit(temp[i]) && i >= 0; i--)
			s = i;
		for(i = middle + 1; isDigit(temp[i]) && i < strlen(temp); i++)
			e = i;
		k = 0;
		for(i = s; i < (int)middle; i++)
				num1[k++] = temp[i];
		num1[k] = '\0';
		k = 0;
		for(i = (int)middle + 1; i <= e; i++)
				num2[k++] = temp[i];
		num2[k] = '\0';
		k = 0;
		one = atof(num1);
		two = atof(num2);
		res = one + two;
		sprintf(n, "%lf", res);
		expreplace(temp, s, e, n);
	}
	flag = 0;
	if(!strcmp(temp, "inf"))
		return "Undefined";
	if(!strcmp(temp, "-inf"))
		return "Undefined";
	if(!strcmp(temp, "nan"))
		return "Not a Number";
	for(i = 0; i < strlen(temp); i++){
		if(!isDigit(temp[i]))
			return "Invalid Input";
	}
	return temp;
}
/*Function to remove white spaces.
 */
void wsr(char *p){
	int i, j = 0;
	char temp[1024] = {0};
	for(i = 0; i < strlen(p); i++){
		if(!(p[i] == ' ' || p[i] == '\t'))
			temp[j++] = p[i];
	}
		strcpy(p, temp);
}
/*called when = is clicked
 */
void on_equals_clicked(){
	int s , e, i = 0, j = 0, k;
	char temp[1024], c, *a, exp[1024], ans[1024] = {0};
	a = (char*)malloc(sizeof(char) * 1024);
	strcpy(a, gtk_entry_get_text(GTK_ENTRY(g_lbl_entry)));
	strcpy(ans, a);
	strcat(ans, "   =   ");
	c = a[i];
	while(i < strlen(a)){
		exp[j++] = c;
		c = a[++i];
	}
	exp[j] = '\0';
	wsr(exp);
	strcpy(a, exp);
	while(1){
		s = e = INT_MIN;
		for(i = 0; i < strlen(a); i++){
			if(a[i] == '(')
				s = i;
			if(a[i] == ')'){
				e = i;			
				break;
			}
		}
		if(s != INT_MIN && e != INT_MIN){
			k = 0;
			for(j = s + 1; j < e; j++)
				temp[k++] = a[j];
			temp[k] = '\0';
			strcpy(res, evaluator(temp));
			expreplace(a, s, e, res);
		}
		else{
			strcpy(res, evaluator(a));
			break;
		}
	}
 /*sets the result to entry and complete 
  *expression with result to the label
  */
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), res);
	strcat(ans, res);
	gtk_label_set_text(GTK_LABEL(g_lbl_label), ans);
	free(a);
}
/*called when del is clicked
 */
void on_del_clicked(){
	const char *del = "";
	char *new_text = malloc(sizeof(char) * 50);
	strcpy(new_text, del);
	gtk_entry_set_text(GTK_ENTRY(g_lbl_entry), new_text);
	free(new_text);
}
/*called when window is closed
 */
void on_window_main_destroy(){
    gtk_main_quit();
}
